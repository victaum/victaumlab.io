#+TITLE: Introducción a Octave
#+AUTHOR: Víctor H. Manotas G.
#+EMAIL: victorma31@gmail.com
#+OPTIONS: num:nil toc:nil
#+DATE: [2020-10-04]
#+REVEAL_ROOT: https://cdn.jsdelivr.net/npm/reveal.js
#+REVEAL_THEME: league
* ¿Qué es Octave?
  Es un sistema de computo especializado en análisis numerico; similar y
  compatible hasta cierto punto con MatLab.
  [[http://www.gnu.org/software/octave][Ver pagina web]].
* Fortalezas
  - Sistema con su propio lenguaje de programación que permite extender
    sus funcionalidades.
  - El lenguaje es interpretado, lo que lo hace más flexible y rápido
    para desarrollar. 
  - Adecuado para problemas de algebra lineal, pues tiene instruciones internas
    especializadas para eso.
  - Maneja internamente el cálculo vectorizado. 
* Debilidades
   - Como su lenguaje es interpretado (script) es menos rápido que un lenguaje
     pre-compilado.
   - No es un lenguaje de programación de proposito general.
* Obtener Octave
** En Linux:
     + Manejador de paquetes:
       En diferentes distribuciónes gnu/linux puede variar pero en sistemas
       basados en debian:
     #+begin_src sh
       sudo apt install octave
     #+end_src
     + Compilar de la fuente:
       Se puede obtener el código fuente de octave de esta [[http://www.gnu.org/software/octave/download.htm][dirección]].
       Una vez obtenida la bola tar debe extraerse:
       #+begin_src sh
       tar -xvf <fuente>.tar.gz
       #+end_src
** En linux ...  
     + Se compila el código:
        #+begin_src shell
         ./configure
         make
        #+end_src
     + Se instala el paquete:
        #+begin_src shell
        make install
        #+end_src
** En Windows:
    Se baja el instalador de la siguiente [[https://www.gnu.org/software/octave/download.html#ms-windows][dirección]]. Se ejecuta y se sigue la
    instalación paso.
* Probando la instalación de Octave
   Simplemente se ejecuta "octave" y aparece el "PROMPT" (Linea de Comandos):
   #+begin_src shell
   Octave> 
   #+end_src
   Se prueba con:
   #+begin_src shell
   Octave> surf(peaks);
   #+end_src
* Configurando Octave
    En construcción...
* Programación en General y Octave 
   La programación a nivel general esta basada en:
** Expresiones Primitivas:
     Estas son las expresiones más básicas de que esta compuesto el lenguaje.
     En "Octave" estas consisten en:
     - Numeros.
       Estos pueden ser reales o complejos.
       Reales: 1.0e-3 .
       Complejos: 1 - 2I .
     - Cadenas de caracteres.
       "octave" .
     - Operadores.
       Estos pueden ser aritmeticos: ( *, +, -, /, ^) ,
       o matriciales: ( *., +., \, ^.) .
** Medios de Combinación 
     Donde expresiones primitivas se combinan para formar otras más complejas:
     - Vectores.
       Listas de números:
       [ 1 2 3 ]
     - Matrices.
       Colecciones de vectores:
       [ 1 2 3 ; 4 5 6; 7 8 9] .
       Estas se pueden guardar en archivos que terminan en ".mat"
     - Estructuras.
       Similares a registros u objetos en otros lenguajes.
       s.a=1; s.b=2; s ==> s={ a=1; b=2}
** Medios de combinación ...
     - Listas celulares:
       Similares a hojas de cálculo en excell.
       Ejemplo:
       cellarray= {1, "hola"}; cellarray ==> cellarray={ [1,1] = 1;
       [1,2] = "hola"}

** Medios de Generalización:
     En donde las expresiones repetitivas se organizan y se llaman con un
     nombre:
     - Guiones/(Scripts):
       Estos son archivos que contienen expresiones de octave a ser ejecutadas
       secuencialmente. Estos terminan en ".m".
     - Funciones/(Functions):
       Similar a los guiones pero estas tienen argumentos de entrada y salida
       especificados.
       Ejemplo:
      #+begin_src octave
function  A = my_square(n)
          A = n*n;
endfunction
      #+end_src
* Familiarizandose con el REPL 
  La palabra "REPL" es un acrónimo en inglés que se refiere a:
  | READ  | LEER     |
  | EVAL  | EVALUAR  |
  | PRINT | REPORTAR |
  | LOOP  | REPETIR  |

* Familiarizandose con el REPL ...
  - (READ): La expresion en octave se escribe en la linea de comandos y se le da al interprete con la tecla <ENTER>.
  - (EVAL): El interprete la evalua.
  - (PRINT): Se obtiene una respuesta que se imprime en pantalla.
  - (LOOP): El interprete espera a que el usuario de otra orden.

  Para entrar en el "REPL" simplemente se ejecuta: "> octave".
  #+BEGIN_NOTES
  Se repasaran las expresiones de "octave" en el REPL para entenderlas
  mejor.  
  #+END_NOTES
